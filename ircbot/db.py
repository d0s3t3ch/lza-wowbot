import datetime
from peewee import *
import config


db = SqliteDatabase(config.SQLITE_DB_PATH)

class BaseModel(Model):
    class Meta:
        database = db

class User(BaseModel):
    irc_nick = CharField()
    account_index = IntegerField()
    address_index = IntegerField()
