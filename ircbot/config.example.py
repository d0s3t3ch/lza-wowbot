# Bot meta
BOT_NICKNAME = 'lza-wowbot'
ROOMS = ['wownero', 'secret-bot-testing-facility']
ADMIN_NICKNAMES = ['lza_menace']

# Wownero Daemon
DAEMON_HOST = 'endpoint or ip address' # ip of the node
DAEMON_PORT = 34568 # port the node accepts for rpc calls
DAEMON_PROTO = 'http' # protocol used for communications
DAEMON_USER = 'username'
DAEMON_PASS = 'password'

# Wownero Wallet
WALLET_HOST = 'endpoint or ip address' # ip of the wallet host
WALLET_PORT = 8888 # port the wallet accepts for rpc calls
WALLET_PROTO = 'http' # protocol used for communications
WALLET_USER = 'username'
WALLET_PASS = 'password'

# Registration
ACTIVE_WALLET_ACCOUNT = 0

# Security
PASSWORD_SALT = 'salt here' # database salts
SECRET_KEY = 'secret session key here' # encrypts the session token

# DB
SQLITE_DB_PATH = './data/wowbot-db.sqlite'

# IRC
IRC_HOST = 'ajnvpgl6prmkb7yktvue6im5wiedlz2w32uhcwaamdiecdrfpwwgnlqd.onion'
TELEGRAM_BRIDGE_NAME = 'WowBridge'
TELEGRAM_JOIN_SUBSTR = 'has joined the Telegram Group'

# MEMES
IMAGES = [
    'https://files.lzahq.tech/wow/memes/44lwio.jpg',
    'https://files.lzahq.tech/wow/memes/1j0lAMZ.png',
    'https://files.lzahq.tech/wow/memes/3IFtDIQ.jpg',
    'https://files.lzahq.tech/wow/memes/49W12pr.png',
    'https://files.lzahq.tech/wow/memes/Cly8ERi.jpg',
    'https://files.lzahq.tech/wow/memes/DK7fepa.jpg',
    'https://files.lzahq.tech/wow/memes/G5lVTQX.jpg',
    'https://files.lzahq.tech/wow/memes/HYzcRr2.png',
    'https://files.lzahq.tech/wow/memes/Kcmaage.png',
    'https://files.lzahq.tech/wow/memes/MS4odnv.gif',
    'https://files.lzahq.tech/wow/memes/MXJ2slS.png',
    'https://files.lzahq.tech/wow/memes/SPhn9u0.png',
    'https://files.lzahq.tech/wow/memes/WQUmuHl.jpg',
    'https://files.lzahq.tech/wow/memes/c16qByl.png',
    'https://files.lzahq.tech/wow/memes/ck7Uav1.jpg',
    'https://files.lzahq.tech/wow/memes/h3PjuVl.jpg',
    'https://files.lzahq.tech/wow/memes/i5I0Qrm.jpg',
    'https://files.lzahq.tech/wow/memes/i9gKtXe.jpg',
    'https://files.lzahq.tech/wow/memes/iSXJluW.jpg',
    'https://files.lzahq.tech/wow/memes/ixvottX.png',
    'https://files.lzahq.tech/wow/memes/o56reOl.jpg',
    'https://files.lzahq.tech/wow/memes/oYB41Hk.png',
    'https://files.lzahq.tech/wow/memes/q7n2rEZ.png',
    'https://files.lzahq.tech/wow/memes/t0CSrfo.png',
    'https://files.lzahq.tech/wow/memes/uDrpKJ8.png',
    'https://files.lzahq.tech/wow/memes/uhQGV7b.jpg',
    'https://files.lzahq.tech/wow/memes/utjI6MW.png',
    'https://files.lzahq.tech/wow/memes/v7clSBi.png',
    'https://files.lzahq.tech/wow/memes/vQ9v2ub.png',
    'https://files.lzahq.tech/wow/memes/wW0ueSf.gif',
    'https://files.lzahq.tech/wow/memes/xyiE9vx.mp4',
]
