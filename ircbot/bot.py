import sys, os
import pydle
import requests
import random
import config
import wownero
import db


class IRCBot(pydle.Client):
    async def on_connect(self):
        for room in config.ROOMS:
            await self.join(room)

    async def on_message(self, target, source, message):
        if source == self.nickname:
            return
        else:
            print(f"Target: {target} - Source: {source} - Message: {message}")

        if self.nickname in message:
            await self.message(target, f"Sup. I'm not very helpful yet, but getting there.")
        elif message.startswith("meme"):
            await self.message(target, random.choice(config.IMAGES))
        elif message.startswith("register"):
            wallet = wownero.Wallet()
            if not wallet.connected:
                await self.message(target, f"My connection to the wallet RPC endpoint is broken so I can't do that right now. Paging: {config.ADMIN_NICKNAMES}")
            if db.User.filter(irc_nick=source):
                await self.message(target, f"You are already registered. FOH.")
            else:
                new_address = wallet.new_address(account=config.ACTIVE_WALLET_ACCOUNT, label=source)
                u = db.User(
                    irc_nick=source,
                    account_index=config.ACTIVE_WALLET_ACCOUNT,
                    address_index=new_address[0]
                )
                u.save()
                print({"type": "registration", "user": source, "address_index": new_address[0]})
                await self.message(target, f"Grats bra, you're locked and loaded. Send Wownero to {new_address[1]}")
        elif message.startswith("deposit"):
            wallet = wownero.Wallet()
            if not wallet.connected:
                await self.message(target, f"My connection to the wallet RPC endpoint is broken so I can't do that right now. Paging: {config.ADMIN_NICKNAMES}")
            if db.User.filter(irc_nick=source):
                u = db.User.get(db.User.irc_nick == source)
                address = wallet.addresses()
                await self.message(target, f"Here's your deposit address {source}: {address[u.address_index]}")
            else:
                await self.message(target, f"You aren't registered. Try saying: `register`")
        elif message.startswith("tip"):
            msg_split = message.split()
            l = len(msg_split)
            if l == 1:
                await self.message(target, "Need more info. `tip <recipient_name> <amount>`")
            elif l == 2:
                await self.message(target, f"Need more info. `tip {msg_split[1]} <amount>`")
            elif l == 3:
                await self.message(target, f"Normally I'd send a tip of {msg_split[2]} Wownero to {msg_split[1]}, but I'm not ready to do that just yet.")
            else:
                await self.message(target, "You're passing way too much shit bro. `tip <recipient_name> <amount>`")
        # elif message.startswith("!proposal "):
        #     split_msg = message.split()
        #     param = split_msg[1]
        #     print(param)
        #     print(split_msg)
        #     if isinstance(param, int):
        #         r = requests.get("https://funding.wownero.com/api/1/proposals?status=2")
        #         if r.status_code == 200:
        #             found = False
        #             for prop in r.json()["data"]:
        #                 if prop["id"] == param:
        #                     found = True
        #                     await self.message(target, f"{prop}")
        #             if not found:
        #                 await self.message(target, f"There's no proposal with that ID")
        #         else:
        #             await self.message(target, f"I didn't get a valid response from the WFS API. Debug: {r.content}")
        #     else:
        #         await self.message(target, f"You're supposed to provide a number ya dingus!")
        elif source == config.TELEGRAM_BRIDGE_NAME and config.TELEGRAM_JOIN_SUBSTR in message:
            # ex = 'DudeMan (@DudeManBro) has joined the Telegram Group!'
            strip_last = message.split()[1][:-1]
            strip_first = strip_last[1:]
            await self.message(target, f"WTF is up {strip_first}! Be sure to give us all your money here (https://funding.wownero.com/proposals) and start shitposting here (https://forum.wownero.com/). Stay tuned for more cool ass bot shit goin down!")

        # isadmin = await self.is_admin(source)
        # print(isadmin)

    async def is_admin(self, nickname):
        admin = False
        if nickname in config.ADMIN_NICKNAMES:
            info = await self.whois(nickname)
            admin = info['identified']
            print("info: ", info)
        return admin



if __name__ == '__main__':
    try:
        print(f"[+] Starting IRC bot connecting to {config.IRC_HOST}...\n")
        client = IRCBot(
            nickname=config.BOT_NICKNAME,
            sasl_mechanism = "EXTERNAL",
            tls_client_cert = "./freenode.pem",
        )
        client.run(config.IRC_HOST, tls=True, tls_verify=False)
    except KeyboardInterrupt:
        print(' - Adios')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
