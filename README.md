## lza-wowbot

Doing Rust for everything was a PITA, so I'm going back to my roots and just doing a simple Python thing. Keep it ez anyways.

## Usage

Not exactly the smoothest initialization for the bot right now; it's very stateful. You will need to spend some time creating an IRC account, generating keys, pulling container images, and setting up a wallet.

```
# pull latest wownero image
docker pull lalanza808/wownero

# generate new keys
openssl req -x509 -sha256 -new -newkey rsa:4096 -days 1000 -nodes -out freenode.pem -keyout freenode.pem

# get fingerprint
openssl x509 -in freenode.pem -outform der | sha1sum -b | cut -d' ' -f1

# use web IRC and authenticate
# /msg NickServ CERT ADD xxxxxxxxxxxxxxx

# initialize new wallet and retain seed
docker run --rm -it --name wow-wallet-init \
  -v $(pwd)/data:/root \
  wownero \
  wownero-wallet-cli \
  --daemon-address https://node.suchwow.xyz:443 \
    --generate-new-wallet /root/wow \
    --password testing \

# setup rpc process
docker run --rm -d --name wow-wallet \
  -v $(pwd)/data:/root \
  -p 8888:8888 \
  wownero \
  wownero-wallet-rpc \
    --daemon-address https://node.suchwow.xyz:443 \
    --wallet-file /root/wow \
    --password testing \
    --rpc-bind-port 8888 \
    --rpc-bind-ip 0.0.0.0 \
    --confirm-external-bind \
    --rpc-login xxxx:yyyy \
    --log-file /root/rpc.log

# setup tor process
pushd torsocks && docker build -t tor . && popd
docker run -p 9050:9050 --rm -d --name tor tor

# install torsocks
brew install torsocks

# install python dependencies
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt

# use remote relay/bridge
torsocks python3 bot.py
```
